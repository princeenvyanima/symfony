<?php

    namespace App\Service;

    use App\Entity\User;
    use Psr\Log\LoggerInterface;
    use Symfony\Bridge\Twig\Mime\TemplatedEmail;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Mailer\MailerInterface;
    use Symfony\Component\Mime\Address;
    use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
    use Throwable;
    use function fopen;
    use function sprintf;

    class RegistrationEmailSendler
    {
        private const SUBJECT_TITLE = 'Вы успешно зарегистрированы';
        private MailerInterface $mailer;
        private LoggerInterface $logger;

        public function __construct(MailerInterface $mailer, LoggerInterface $logger) {

            $this->mailer = $mailer;
            $this->logger = $logger;
        }
        public function SendSuccessRegistrationEmail(User $user):void {

            $prepared = $this->prepareEmail($user);
            try {
                $this->mailer->send($prepared);
            } catch (TransportExceptionInterface $exception) {
                $this->logger->critical($exception->getMessage());
            }

        }

        private function prepareEmail(User $user): TemplatedEmail {
            $email = new TemplatedEmail();
            $email
                ->addTo(new Address($user->getEmail(), $user->getFullName()))
                ->subject(self::SUBJECT_TITLE)
                ->context([
                    'name' => $user->getFullName(),
                    'login' => $user->getEmail(),
                ])
                ->htmlTemplate('email/success-registration.html.twig')
            ;
            return $email;

        }
    }