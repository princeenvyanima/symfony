<?php

    namespace App\Controller;

    use App\Repository\UserRepository;
    use App\Service\RegistrationEmailSendler;
    use Exception;
    use Symfony\Bridge\Twig\Mime\TemplatedEmail;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Mailer\MailerInterface;
    use Symfony\Component\Mime\Email;
    use Symfony\Component\Routing\Annotation\Route;
    use Throwable;
    use function sprintf;

    class EmailController extends AbstractController
    {
        /**
         * @param Request $request
         * @return Response
         * @Route(path="/send-email", name="app.send_email")
         */
        public function indexAction(UserRepository $userRepository, RegistrationEmailSendler $emailSendler): Response {

            $user = $userRepository->findOneBy(['id' => 1]);

            try {
                $emailSendler->SendSuccessRegistrationEmail($user);
            } catch (Throwable $exception) {
                return new Response('error');
            }
            return new Response('success');

        }
    }